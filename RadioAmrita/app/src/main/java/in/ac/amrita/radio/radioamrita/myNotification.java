package in.ac.amrita.radio.radioamrita;

import android.graphics.Bitmap;

import co.mobiwise.library.NotificationManager;
import co.mobiwise.library.RadioPlayerService;

/**
 * Created by Santhosh on 10/23/2015.
 */
public class myNotification extends NotificationManager {
    public myNotification(RadioPlayerService service) {
        super(service);
    }

    @Override
    public void startNotification(String radioName, String trackInformation, Bitmap artImage) {
        super.startNotification(radioName, trackInformation, artImage);

    }

    @Override
    public void updateNotificationMediaData(String radioName, String trackInformation, Bitmap artImage) {
        super.updateNotificationMediaData(radioName, trackInformation, artImage);


    }


    @Override
    public void updateNotificationMediaData(String radioName, String trackInformation, int artImage) {
        super.updateNotificationMediaData(radioName, trackInformation, artImage);


    }
}
