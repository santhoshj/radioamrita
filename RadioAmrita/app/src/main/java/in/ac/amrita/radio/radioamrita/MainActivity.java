package in.ac.amrita.radio.radioamrita;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.session.MediaSessionManager;
import android.os.SystemClock;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Chronometer;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import co.mobiwise.library.RadioListener;
import co.mobiwise.library.RadioManager;

public class MainActivity extends AppCompatActivity implements RadioListener, AudioManager.OnAudioFocusChangeListener {

    private static String Radio_URL = "http://192.168.137.231:8000";


    public static final String ACTION_PLAY = "action_play";
    public static final String ACTION_PAUSE = "action_pause";

    RadioManager mRadioManager = RadioManager.with(this);
    private Chronometer ch;

    private static ImageButton play_button;
    private ImageView im;
    private AudioManager am;
    TextView title, artist;


    private MediaSessionManager mManager;
    public MediaSessionCompat mSession;
    private MediaControllerCompat mController;
    private Bitmap icon;
    private String tit = "";
    private boolean state = false;
    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Invoke it #onCreate
        mRadioManager.registerListener(this);
        mRadioManager.enableNotification(true);

        play_button = (ImageButton) findViewById(R.id.play);
        ch = (Chronometer) findViewById(R.id.chronometer);

        title = (TextView) findViewById(R.id.album_title);
        artist = (TextView) findViewById(R.id.album_artist);

        FrameLayout fm = (FrameLayout) findViewById(R.id.album_art);

        pd = new ProgressDialog(this);
        pd.setMessage("Connecting...");

        im = (ImageView) findViewById(R.id.imageView);
        im.setMinimumHeight(im.getWidth());
        play_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mRadioManager.isPlaying()) {
                    Stop_Streaming();

                } else {

                    if (get_AudioFocus()) {
                        Start_Streaming();
                    }
                }

            }
        });

    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i("status", "onstop");

        if (mRadioManager.isPlaying()) {
            state = true;
            SharedPreferences sh = getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor mEditor = sh.edit();
            mEditor.putBoolean("state", true);
            mEditor.putString("title", tit);
            mEditor.apply();
        } else {

            state = false;
            SharedPreferences sh = getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor mEditor = sh.edit();
            mEditor.putBoolean("state", false);
            mEditor.putString("title", "Radio Amrita");
            mEditor.apply();

        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        Log.i("status", "onrestart");
    }

    public void initMediaSession() {


        mManager = (MediaSessionManager) getSystemService(MEDIA_SESSION_SERVICE);

        ComponentName Mediabuttonreciver = new ComponentName(this, MediaControlReciever.class);
        mSession = new MediaSessionCompat(getApplicationContext(), "radio", Mediabuttonreciver, null);

        mSession.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS | MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
        mSession.setCallback(new MediaSessionCompat.Callback() {
            @Override
            public void onPlay() {
                super.onPlay();
                Log.e("mediacompact callback", "onplay");

            }

            @Override
            public void onPause() {
                super.onPause();
                Log.e("mediacompact callback", "onplay");

            }
        });

        mSession.setActive(true);

    }

    private NotificationCompat.Action generateAction(int icon, String title, String intentAction) {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setAction(intentAction);
        PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 1, intent, 0);
        return new NotificationCompat.Action.Builder(icon, title, pendingIntent).build();
    }

    public void Show_notification() {

        NotificationCompat.MediaStyle mStyle = new NotificationCompat.MediaStyle();
        mStyle.setShowCancelButton(true);

        mStyle.setMediaSession(mSession.getSessionToken());
        icon = BitmapFactory.decodeResource(this.getResources(),
                R.drawable.radioam);

        NotificationCompat.Builder mBuilder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.noti_icon)
                        .setLargeIcon(icon)
                        .addAction(generateAction(R.drawable.play, "play", ACTION_PLAY))
                        .setContentTitle("My notification")
                        .setContentText("Hello World!").setStyle(mStyle);


        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        notificationManager.notify(1, mBuilder.build());


    }

    private boolean get_AudioFocus() {

        am = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);

        // Request audio focus for playback
        int result = am.requestAudioFocus(this,
                // Use the music stream.
                AudioManager.STREAM_MUSIC,
                // Request permanent focus.
                AudioManager.AUDIOFOCUS_GAIN);

        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            // am.registerMediaButtonEventReceiver(RemoteControlReceiver);
            // Start playback.
            return true;
        } else
            return false;


    }

    private void load_image() {
        Picasso.with(this)
                .load(Radio_URL + "/playingart?sid=1")
                .placeholder(R.drawable.ra).fit()
                .error(R.drawable.ra).skipMemoryCache()
                .into(im);

    }

    @Override
    protected void onStart() {
        super.onStart();
        mRadioManager.connect();
        Log.i("position ", "onstart");

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Log.i("position ", "Back pressed");
        // Start_Streaming();
        //finish();

        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);

        // this.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sh = getPreferences(Context.MODE_PRIVATE);
        state = sh.getBoolean("state", false);
        tit = sh.getString("title", "Radio Amrita");
        Log.i("position ", "onResume " + state);
        if (state) {
            load_image();
            play_button.setImageResource(R.drawable.btn_playback_pause);
            title.setText(tit);
        }
        SharedPreferences.Editor mEditor = sh.edit();
        mEditor.putBoolean("state", false);
        mEditor.apply();


    }

    public void Stop_Streaming() {
        if (mRadioManager.isPlaying()) {
            mRadioManager.stopRadio();
            ch.stop();
            ch.setBase(SystemClock.elapsedRealtime());
            Log.d("status ", "radio stopped");
        }
    }

    public void Start_Streaming() {

        if (new Connectiondectector(this).isConnectingToInternet()) {
            if (!mRadioManager.isPlaying()) {
                mRadioManager.startRadio(Radio_URL);
              pd.getCurrentFocus();
                pd.show();
            }
        } else {

            Show_dialog("");
        }
    }

    public void Show_dialog(String msg) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Error").setIcon(android.R.drawable.stat_notify_error);
        builder.setMessage("Internet Connection Unavailable !");

        AlertDialog dialog = builder.create();
        dialog.show();


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRadioManager.disconnect();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRadioConnected() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //pd.show();

            }
        });
        Log.i("status", "Radio connected");
    }

    @Override
    public void onRadioStarted() {
        Log.i("status", "Radio started");
        runOnUiThread(new Runnable() {

            public void run() {
                play_button.setImageResource(R.drawable.btn_playback_pause);
                ch.setBase(SystemClock.elapsedRealtime());

                pd.dismiss();

                ch.start();
            }
        });


    }

    @Override
    public void onRadioStopped() {
        Log.i("status", "Radio stopped");

        runOnUiThread(new Runnable() {

            public void run() {
                play_button.setImageResource(R.drawable.btn_playback_play);
                title.setText("Radio Amrita");
                im.setImageResource(R.drawable.ra);
            }
        });

    }

    @Override
    public void onMetaDataReceived(String s, String s1) {
        Log.i(s, s1);
        if (s != null)
            if (s.equals("StreamTitle")) {
                tit = s1;
            }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                title.setText(tit);
                title.setSingleLine(true);
                load_image();
            }
        });

    }

    @Override
    public void onAudioFocusChange(int focusChange) {

        if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT) {
            // Pause playback
            Stop_Streaming();
        } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
            // Resume playback
            //  Start_Streaming();
        } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
            //am.unregisterMediaButtonEventReceiver(RemoteControlReceiver);
            //am.abandonAudioFocus(afChangeListener);
            // Stop playback
            Stop_Streaming();
        }

    }

    public class MediaControlReciever extends BroadcastReceiver {


        @Override
        public void onReceive(Context context, Intent intent) {

        }
    }

}
